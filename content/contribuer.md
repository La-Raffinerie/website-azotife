---
title: Contribuer
date: 2021-03-13T08:30:05.000+04:00

---
#### Animer un atelier

* [Positionnement et dispo sur l'animation d'atelier dans le cadre azotifé](https://forms.gle/hkrNrmbdPnDLEQto6)

Ce questionnaire a été créé afin de vous permettre de vous positionner et de donner vos disponibilités sur des ateliers dans le cadre de l'action Azotifé. Nous vous demanderons également de participer à la partie rédactionnelle de la ou les fiches tutos qui servira lors de l'atelier.

#### Proposer une fiche tuto

* [rédactionnel des fiches tutos](https://docs.google.com/forms/d/e/1FAIpQLSfbMI8o0E14xpCwwisn2IHqheQVHTMKhPZkaaJCyGpCzJxvTQ/viewform)

Ce **questionnaire** a été créé afin de vous permettre de proposer des ateliers et des fiches tutos dans le cadre de l'action **Azotifé**. Le but est d'avoir tous les éléments afin de pouvoir passer la main au chargés de mission graphiste pour la partie illustration et mise en page. Si vous avez plusieurs fiches tutos, il faut remplir plusieurs questionnaires, merci!