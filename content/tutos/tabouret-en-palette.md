---
date_creation: 2020-01
prix: 12€
dechets: 5
age_minimum: 15
contact: laMicrorecyclerie
title: Tabouret en palette
image: uploads/icone-tabouret-en-palette.jpeg
auteur: Julien Gaillot
categories:
- Bricolage
tags:
- palette
- meuble
difficulte: 3
temps: 2/3h
pdf: uploads/brico-02.pdf
tutos_en_lien:
- tutos/travailler-la-palette.md
- tutos/depalettiser.md

---
