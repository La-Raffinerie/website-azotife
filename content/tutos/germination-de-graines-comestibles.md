---
date_creation: 2020-04
prix: 1€
dechets: 0
age_minimum: 3
contact: leLaboratoireDeTransformationAlimentaire
date: 2021-03-19T14:02:27+04:00
title: Germination de graines comestibles
image: uploads/icone-germination-de-graines-comestibles.jpeg
auteur: goutnature.re
categories:
- Recette
tags:
- cuisine
difficulte: 1
temps: 10 min
origine: ''
pdf: uploads/recette-01.pdf
tutos_en_lien: []

---
