---
date_creation: 2021-03
prix: 5 euros
dechets: 0.3
age_minimum: 5
title: Gâteau aux bananes très mûres
image: ''
auteur: Hélène
categories:
- Recette
tags:
- dessert
- goûter
- anti gaspi
- cuisine
difficulte: 2
temps: 20 min (+45 min de cuisson)
origine: ''
pdf: ''
tutos_en_lien: []

---
_Cette recette est idéale pour éviter de gaspiller des bananes très (trop) mûres. Idéal pour le goûter des enfants, pour le petit-déjeuner ou pour le dessert._

Ingrédients :

* 3 bananes trop mûres, écrasées
* 3 œufs
* 210 g de farine
* 150 g de sucre roux
* 125  g de beurre fondu
* 1 sachet de levure chimique
* 60 mL de lait
* Optionnel : de la cannelle en poudre, de l'extrait de vanille, des pépites de chocolat, des noix de pécan...

Préparation :

1. Préchauffer le four à 160°C.
2. Graisser un moule à cake avec du beurre fondu.
3. Mélanger la farine, le sucre et la levure dans un saladier.
4. Dans un autre saladier, mélanger le beurre fondu, les œufs, le lait et les bananes écrasées. 
5. Ajouter les ingrédients optionnels en fonction de vos envies.
6. Faire un puit au centre des ingrédients secs et verser la préparation humide. Mélanger.
7. Verser la pâte dans le moule, faire cuire 45 minutes.
8. Déguster.