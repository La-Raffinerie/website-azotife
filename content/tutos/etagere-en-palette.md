---
date_creation: 2021-03
prix: 16€
dechets: 35
age_minimum: 15
contact: laMicrorecyclerie
title: Etagère en palette
image: uploads/icone-etagere-en-palette.svg
auteur: Julien Gaillot
categories:
- Bricolage
tags:
- palette
- meuble
difficulte: 3
temps: 3/4h
pdf: uploads/brico-01-1.pdf
tutos_en_lien:
- tutos/travailler-la-palette.md
- tutos/depalettiser.md

---
