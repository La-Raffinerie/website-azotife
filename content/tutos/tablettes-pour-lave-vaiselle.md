---
date_creation: 2021-03
prix: 5€
dechets: 
age_minimum: 
contact: Azotife
date: 2021-03-22T21:33:51.000+04:00
title: Tablettes pour lave vaiselle
image: uploads/tablettes-pour-lave-vaiselle.jpg
auteur: Virginie Leguy
categories:
- Recette
tags:
- hygiène
- maison
- ménage
difficulte: 1
temps: 15 minutes
origine: ''
pdf: uploads/fiche-tuto-tablettes-pour-lave-vaiselme.pdf
tutos_en_lien: []

---
#### Description

Nous vous proposons ici de réaliser vos propres tablettes pour lave vaisselle. 

#### 1ère étape

Rassembler les matériaux :

* 100 g de bicarbonate de soude
* 100 g de cristaux de soude
* 100 g de gros sel
* 100 g d'acide citrique
* Un peu d'eau

Prévoir les outils : un bocal hermétique vide, un vaporisateur, un saladier et un bac à glaçons en silicone.

#### Méthodes

* Mélanger tous les ingrédients dans un saladier.
* Pulvériser un peu d'eau et mélanger pour éviter les grumeaux, jusqu’à obtention d’une pâte poudreuse qui s'effrite facilement.
* Répartir le mélange dans le bac à glaçons (bien tasser).
* Laisser reposer 24 heures, puis démouler et stocker dans un bocal hermétique.