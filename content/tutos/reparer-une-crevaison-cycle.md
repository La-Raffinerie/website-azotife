---
date_creation: 2021-04
prix: 2€
dechets: 2
age_minimum: 6
contact: espaceSportifs
title: Réparer une crevaison cycle
image: uploads/cycle-repar.png
auteur: Fabien CARTIER-MOULIN
categories:
- Bricolage
tags:
- cycle
- velo
difficulte: 2
temps: 15 min
pdf: uploads/reparer-une-crevaison.pdf
tutos_en_lien: []

---
En fiche Pdf jointe une des très détaillées explications du WIKLOU, le Wikipédia du biclou pour réparer une crevaison cycle-[ plusieurs versions ici.](https://wiklou.org/wiki/Crevaison "Fiches tuto crevaison")