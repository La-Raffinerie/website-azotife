---
date_creation: 2021-03
prix: 0.1€
dechets: 0.2
age_minimum: 5
title: Chips croustillantes d'épluchures de légumes
image: uploads/epluchures.jfif
auteur: Hélène
categories:
- Recette
tags:
- apéro
- bio
- zéro déchets
- cuisine
difficulte: 1
temps: 20 minutes
origine: ''
pdf: ''
tutos_en_lien: []
contact: leLaboratoireDeTransformationAlimentaire

---
#### Ingrédients :

* Epluchures de légumes bio (pommes de terre, carottes, courgettes, aubergines, betteraves...)
* Huile d'olive
* Sel et poivre
* Epices et herbes
 
#### Etapes :

1. Préchauffer le four à 180° C.
2. Dans un saladier, déposer les épluchures de légumes et les enrober d'huile d'olive.
3. Assaisonner avec du sel et du poivre.
4. Ajouter des épices pour parfumer les chips : curry, curcuma, piment doux, thym...
5. Déposer les épluchures sur une plaque de cuisson chemisée d'un papier sulfurisé, et enfourner pour 15 minutes.
6. Une fois dorées et croustillantes, sortir les chips du four. A déguster tout de suite ou à conserver jusqu'à 5 jours dans une boîte hermétique.

_Ces chips croquantes et colorées peuvent s'accompagner avec du pesto de fanes de radis par exemple._