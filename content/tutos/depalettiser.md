---
date_creation: 2021-03
prix: 0€
dechets: 0
age_minimum: 0
contact: laMicrorecyclerie
test:
- afaaf
- "`fa"
- vdazvsdvdz
- vzdsv
title: Dépalettiser
image: uploads/icone-depalettiser.jpeg
auteur: Julien Gaillot
categories:
- Bricolage
tags: []
difficulte: 3
temps: 15 min
pdf: uploads/brico-a.pdf
tutos_en_lien: []

---
