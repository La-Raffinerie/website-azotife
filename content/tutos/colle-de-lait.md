---
date_creation: 2021-03
prix: 1€
dechets: 
age_minimum: 10
title: Colle de lait
image: uploads/icone-colle-de-lait.jpeg
auteur: ''
categories:
- Recette
tags:
- bocaux
difficulte: 1
temps: 10 min
origine: ''
pdf: uploads/recette-04.pdf
tutos_en_lien: []
contact: laMicrorecyclerie

---
Colle au lait pour étiqueter les confitures et les bocaux faits maison Pour coller simplement des étiquettes en papier sur des pots de confiture ou des bocaux maison