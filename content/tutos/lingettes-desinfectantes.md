---
date_creation: 2021-03
prix: 2€
dechets: 
age_minimum: 
title: Lingettes désinfectantes
image: uploads/lingettes.jpg
auteur: Virginie Leguy
categories:
- Recette
tags:
- maison
- hygiène
- huiles essentielles
- ménage
difficulte: 1
temps: 5 minutes
pdf: uploads/fiche-tuto-lingettes-desinfectantes.pdf
tutos_en_lien: []
origine: ''
contact: laMicrorecyclerie
date: 

---
#### Description

Il s'agit ici de réaliser ses propres lingettes désinfectantes. Cette recette est très facile à réaliser et permet de donner une deuxième vie à des bouts de tissus dont vous ne savez pas quoi faire. 

#### 1ère étape

Rassembler les matériaux :

* 250 ml d'eau
* 250 ml de vinaigre blanc
* 1 cuillère à café de bicarbonate de soude
* 10 gouttes d'huile essentielle (citron, eucalyptus, tea-tree)
* Des écorces d'orange et de citron

Prévoir les outils : un bocal hermétique vide et des bouts de tissu de petite taille.

#### Méthodes

* Mettre tous les ingrédients dans le bocal.
* Mettre 5 ou 6 bouts de tissu dans le bocal, bien les immerger .
* Et voilà, vos lingettes sont prêtes !!