---
date_creation: 2021-03
prix: 0,5€
dechets: 0.1
age_minimum: 8
contact: leCafeRestaurant
date: 2021-03-30T19:10:10+04:00
title: Boisson fraîche aux pelures de gingembre et de concombre et aux zestes de citron
image: uploads/recette-eau-detox-3-1536x1024.webp
auteur: Hélène
categories:
- Recette
tags:
- gingembre
- rafraichissant
- récup
- boisson
difficulte: 1
temps: 3 min
origine: ''
pdf: ''
tutos_en_lien: []
draft: true

---
Une boisson rafraichissante, à réaliser avec des ingrédients bio car on en utilise la peau.

INGREDIENTS (4 verres) :

* 1.5l d’eau
* 1 concombre bio (épluchure)
* 1 citron bio (zeste)
* 1 gingembre bio (pelure)
* Quelques feuilles de menthe

ETAPES :

1. Eplucher le concombre et le gingembre et zester le citron.
2. Déposer tous ces éléments au fond d’une carafe et écraser légèrement à l’aide d’un pilon.
3. Verser de l’eau fraiche sur le dessus et ajouter quelques feuilles de menthe grossièrement froissées.
4. Idéalement, laisser infuser 2h au réfrigérateur.