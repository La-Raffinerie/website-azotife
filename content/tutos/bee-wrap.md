---
date_creation: 2021-03
prix: 4€
dechets: 1
age_minimum: 15
contact: Azotife
date: 2021-03-29T19:53:12+04:00
title: Bee-wrap
image: uploads/beewrap.jpg
auteur: Chloé
categories:
- Recette
tags:
- Cire d'abeille
- Beewrap
- Cuisine
- Simple
- Tissu
difficulte: 2
temps: 20mins
origine: ''
pdf: uploads/fiche-tuto-beewrap.pdf
tutos_en_lien: []

---
