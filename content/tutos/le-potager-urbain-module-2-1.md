---
date_creation: 2021-04
prix: 20€
dechets: 0
age_minimum: 12
contact: Azotife
title: Le potager urbain (Module 2)
image: uploads/potager-urbain-2.jpg
auteur: Kat Bouchez
categories:
- Jardin
tags:
- plantes
- insectes
- 'jardinage '
difficulte: 2
temps: 1h30
pdf: uploads/fiche-tuto-potager-urbain-2.pdf
tutos_en_lien:
- tutos/semer-des-graines-en-pot.md
- tutos/le-potager-urbain-module-1.md
- tutos/le-bouturage.md

---
#### Description

Les 1ères expériences en potager génèrent souvent des émois : attaques de cochenilles, limaces, tang... mais quelle chance ! C'est la vie qui s'exprime ! Comment partager avec elle pour pouvoir agrémenter votre Virgin Mojito de quelques feuilles de menthe ?

#### Tout d'abord, dynamiser votre potager

Lui fournir toutes les conditions de réussite, c'est lui apporter de l'ombre, de l'eau, de l'engrais...

Le purin de bananes = fertilisant

Le purin de fougère =....

Prévoir les outils : coupe-coupe, bêche, petite pioche, marteau...

#### Connaitre ses alliés

Animaux : Tangues, larves de Cétoines, coccinelles, chauve-souris...

Végétaux : Aromatiques, capucines, oeillets d'Inde...

...Et la richesse de la biodiversité

#### Lutter contre les insectes

Cochenilles

Thrips

Pucerons

Oïdium