---
date_creation: 2021-03
prix: 5€
dechets: 0.2
age_minimum: 7
contact: leCafeRestaurant
date: 2021-03-30T18:15:14+04:00
title: Galettes végétales aux restes de lentilles
image: uploads/lentilles-1.jfif
auteur: Hélène
categories:
- Recette
tags:
- végétarien
- recup
- lentilles
- galettes
difficulte: 3
temps: 15 min (+15 min de cuisson)
origine: Inde
pdf: ''
tutos_en_lien: []

---
INGREDIENTS (4 personnes) :

* 150 g de lentilles cuites (lentille corail ou autre type de lentilles)
* 100 g de carottes râpées
* 1 oignon râpé
* restes d'herbes fraiches : coriandre, persil, thym
* 60g de chapelure
* 2 cuillère à soupe d'huile d'olive
* épices : cumin, paprika, curry, sel, poivre

ETAPES :

1. Mettre les lentilles dans une casserole avec un petit peu d'eau et les herbes hachées.
2. Faire chauffer à feu moyen jusqu'à absorption.
3. Enlever la casserole du feu.
4. Ajouter les carottes et les oignons râpés, 3/4 de la chapelure, les épices et bien mélanger.
5. Faire des galettes ou des boulettes avec le mélange ainsi obtenu. Ajouter un peu de farine ou de fécule de maïs si la pâte est trop liquide.
6. Rouler dans la chapelure restante.
7. Faire chauffer l'huile d'olive dans une poêle, faire dorer les galettes quelques minutes de chaque côté.

_Vous pouvez manger les galettes avec une salade, les accompagner d'une sauce au yaourt, ou encore les utiliser comme galettes végétales dans un hamburger._