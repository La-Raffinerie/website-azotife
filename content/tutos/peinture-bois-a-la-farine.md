---
date_creation: 2020-04
prix: 10€
dechets: 0.5
age_minimum: 15
contact: laMicrorecyclerie
date: 2021-03-19T14:17:10+04:00
title: Peinture bois à la farine
image: uploads/icone-peinture-bois-a-la-farine.jpeg
auteur: Johanna Grégoire
categories:
- Bricolage
- Recette
tags:
- couleur
- peinture
difficulte: 1
temps: 1h
origine: ''
pdf: uploads/recette-02.pdf
tutos_en_lien: []

---
