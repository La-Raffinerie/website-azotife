---
date_creation: 2021-03
prix: 0€
dechets: 0
age_minimum: 0
contact: laMicrorecyclerie
title: Travailler la palette
image: uploads/icone-depalettiser.jpeg
auteur: Julien Gaillot
categories:
- Bricolage
tags: []
difficulte: 1
temps: ''
pdf: uploads/brico-b.pdf
tutos_en_lien:
- tutos/depalettiser.md

---
