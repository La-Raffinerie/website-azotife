---
date_creation: 2020-04
prix: 3€
dechets: 0.5
age_minimum: 10
contact: laMicrorecyclerie
date: 2021-03-19T17:05:54+04:00
title: Peinture à la pomme de terre
image: uploads/icone-peinture-a-la-pomme-de-terre.jpeg
auteur: Johanna Grégoire
categories:
- Bricolage
- Recette
tags:
- couleur
- peinture
difficulte: 1
temps: 30 min
origine: ''
pdf: uploads/recette-03.pdf
tutos_en_lien: []

---
