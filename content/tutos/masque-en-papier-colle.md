---
date_creation: 2020-04
prix: 5€
dechets: 0
age_minimum: 6
contact: Azotife
title: Masque en papier collé
image: uploads/icone-masque-en-papier-colle.jpeg
auteur: Claire Mezailles
categories:
- Marmay
tags: []
difficulte: 2
temps: 3h
pdf: uploads/mar-01.pdf
tutos_en_lien:
- tutos/peinture-bois-a-la-farine.md

---
