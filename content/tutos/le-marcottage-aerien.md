---
date_creation: 2020-04
prix: 5€
dechets: 0
age_minimum: 12
contact: PotagerVerger
title: Le marcottage aérien
image: uploads/icone-le-marcottage-aerien.jpeg
auteur: Julien Gaillot
categories:
- Jardin
tags:
- multiplication
difficulte: 2
temps: 15 min
pdf: uploads/jardin-01.pdf
tutos_en_lien: []

---
