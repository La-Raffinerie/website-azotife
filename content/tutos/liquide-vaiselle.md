---
date_creation: 2021-03
prix: 4/5€
dechets: 
age_minimum: 
contact: Azotife
date: 2021-03-20T11:08:38.000+04:00
title: Liquide vaiselle
image: uploads/liquide-vaiselle.jpg
auteur: Virginie Leguy
categories:
- Recette
tags:
- maison
- huiles essentielles
- ménage
- hygiène
difficulte: 1
temps: 15 minutes
origine: ''
pdf: uploads/fiche-tuto-liquide-vaiselle.pdf
tutos_en_lien: []

---
#### Description

Il s'agit ici de réaliser son propre liquide vaisselle maison avec des ingrédients sains. 

#### 1ère étape

Rassembler les matériaux :

* 500 ml d'eau
* 160 ml de savon de Marseille
* 1 cuillère à café de cristaux de soude
* 1 cuillère à café de glycérine végétale
* 1 cuillère à soupe d'aloe vera (ou un morceau de 2 cm)
* 10 gouttes d'huile essentielle de chaque : tea-tree, menthe poivrée, pamplemousse

Prévoir : un récipient adapté (type ancien bidon de liquide vaisselle), et une casserole.

#### Méthodes

* Mettre tous les ingrédients (sauf les huiles essentielles) dans une casserole. 
* Faire bouillir et laisser fondre au maximum.
* Laissez tiédir.
* Ajouter les huiles essentielles au mélange.
* Mettre dans un contenant adapté, et hop ! C’est prêt !