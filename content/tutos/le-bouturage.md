---
date_creation: 2020-04
prix: 5€
dechets: 0
age_minimum: 8
contact: PotagerVerger
title: Le bouturage
image: uploads/icone-le-bouturage.jpeg
auteur: Julien Gaillot
categories:
- Jardin
tags:
- multiplication
difficulte: 2
temps: 15 min
pdf: uploads/jardin-04.pdf
tutos_en_lien: []

---
