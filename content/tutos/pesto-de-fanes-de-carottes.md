---
date_creation: 2021-03
prix: 4€
dechets: 0.2
age_minimum: 8
contact: leCafeRestaurant
date: 2021-03-30T18:46:40+04:00
title: Pesto de fanes de carottes
image: uploads/60de6b83c33462aa946fb0aee386e356-pesto-buffet.jpg
auteur: Hélène
categories:
- Recette
tags:
- Carottes
- Récup
- Apéro
- Végétarien
difficulte: 2
temps: 10 min
origine: ''
pdf: ''
tutos_en_lien: []

---
INGREDIENTS (6-8 personnes) :

* les fanes de 5 carottes hachées grossièrement
* 15g de pignons ou amandes ou noix
* 50 g de parmesan râpé
* jus d'1 citron
* 1 gousse d'ail
* 60 mL d'huile d'olive
* Sel et poivre

ETAPES : 

1. Mettre tous les ingrédients dans un mixeur et mixer jusqu'à obtenir un mélange homogène.
2. Transférer dans un bocal et mettre un filet d'huile d'olive.
3. Consommer le pesto dans les trois jours avec conservation au réfrigérateur ou le placer au congélateur pour l'utiliser ultérieurement.

_Le pesto de fanes de carottes peut accompagner des crudités, se tartiner sur du pain, ou encore servir de sauce pour les pâtes._