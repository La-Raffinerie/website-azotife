---
date_creation: 2021-03
prix: 8 euros
dechets: 0.3
age_minimum: 5
contact: leCafeRestaurant
date: 2021-03-30T17:45:56+04:00
title: Banana bread aux bananes trop mûres
image: uploads/recette-banana-bread-730x520-jpg.webp
auteur: Hélène
categories:
- Recette
tags:
- gâteau
- petit-déjeuner
- goûter
- gourmand
- bananes
difficulte: 2
temps: 15 min (+45 min de cuisson)
origine: Etats-Unis
pdf: ''
tutos_en_lien: []

---
_Le banana bread a été inventé dans les années 1930 aux États-Unis. Ce gâteau à base de bananes bien mûres est simple à cuisiner et se déguste aussi bien au petit-déjeuner qu'au goûter._

INGREDIENTS (6-8 parts) :

* 3 grosses bananes très (trop) mûres
* 200 grammes de farine
* 150 grammes de sucre roux
* 125 grammes de beurre fondu
* 1 sachet de levure
* 1/2 cuillère de bicarbonate de sodium
* 2 œufs
* 60 mL de lait
* optionnel pour les gourmands : du sucre vanillé ou de l'extrait de vanille, de la cannelle,  des pépites de chocolat, des fruits secs en morceaux (noix, amandes, noix de pécan...)

ETAPES :

1. Préchauffer le four à 160°C et graisser un moule à cake.
2. Mélanger la farine, le sucre et le bicarbonate dans un saladier.
3. Dans un autre saladier, mélanger  le beurre, les œufs, le lait et 2 bananes écrasées.
4. Faire un puit au centre des ingrédients secs et y verser les ingrédients humides.
5. Ajouter éventuellement les ingrédients optionnels.
6. Bien mélanger puis verser la pâte dans le moule.
7. Décorer le gâteau en y déposant la banane restante coupée en rondelles.
8. Faire cuire 45 à 50 minutes. Vérifier la cuisson avec la pointe d'un couteau.
9. Déguster.