---
date_creation: 2020-04
prix: 30€
dechets: 0
age_minimum: 15
contact: Azotife
date: 2021-03-19T13:23:08+04:00
title: Fabriquer son savon
image: uploads/icone-fabriquer-son-savon.jpeg
auteur: Laureline Lauret
categories:
- Recette
tags:
- produits ménagers
- cosmétiques
difficulte: 2
temps: 2h
origine: ''
pdf: uploads/recette-05.pdf
tutos_en_lien: []

---
