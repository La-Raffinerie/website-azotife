---
title: Le potager urbain (Module 1)
date_creation: 2021-03
image: uploads/icone-fabriquer-du-sol.jpeg
auteur: Kat Bouchez
pdf: https://static.laraffinerie.re/fiches/JARDIN-02.pdf
categories:
- Jardin
description: Fabriquer du sol, le potager en lasagnes
difficulte: 3
prix: 16€
temps: 2h
tags:
- sol
- potager
age_minimum: 10
dechets: 
tutos_en_lien: []
contact: PotagerVerger

---
#### Description

Il s'agit d'à partir de matériaux facilement trouvables, de constituer une petite planche de potager afin d'y planter quelques salades, aromatiques ou patates.  
Cela peut même être fait sur un sol inerte.

#### 1ère étape

Rassembler les matériaux :

* Pour délimiter la planche : planchettes recyclées, piquets fin, voire cordes
* pour la remplir: matériaux verts (tronc de bananiers, tonte de gazon, votre seau de compost...) / matériaux secs (broyat sec, carton déchiré, feuilles séchées...) et aussi un peu de terre...

Prévoir les outils : coupe-coupe, bêche, petite pioche, marteau...

#### Méthodes

* Placez vos planches ou branches d'arbres sur le sol  à l'aide des piquets pour délimiter un espace à remplir de max 70 cm de large
* Faire des couches secs/verts/secs/verts/secs/verts puis à la fin recouvrir du compost puis de terre et enfin, paillez
* vous pouvez directement planter en godets de terre ou attendre 2 semaines que la planche "prenne".

La vie va peu à peu s'installer dans cette "lasagne", les micro-organismes viendront décomposer le compost et les autres matériaux.