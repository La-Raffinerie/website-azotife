---
date_creation: 2020-04
prix: 60€
dechets: 0
age_minimum: 15
contact: PotagerVerger
title: Une branche à tailler
image: uploads/icone-une-branche-a-tailler.jpeg
auteur: Julien Gaillot
categories:
- Jardin
tags: []
difficulte: 3
temps: 15 min
pdf: uploads/jardin-05.pdf
tutos_en_lien: []

---
